#pragma once

#include <QtWidgets/private/qfusionstyle_p.h>
#include <QtWidgets/private/qfusionstyle_p_p.h>

class SandsmarkStyle : public QFusionStyle
{
    Q_OBJECT

public:
    int styleHint(StyleHint stylehint, const QStyleOption *opt, const QWidget *widget, QStyleHintReturn *returnData) const override;

    // reimplemented to fix focus indication of kiowidgets
    void drawPrimitive(QStyle::PrimitiveElement pe, const QStyleOption *opt, QPainter *p, const QWidget *widget) const override;

    // for improving focus indication of buttons
    void drawControl(QStyle::ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const override;

    // fix slider and combobox focus indication as well as scrollbar groove
    void drawComplexControl(ComplexControl control, const QStyleOptionComplex *option,
            QPainter *painter, const QWidget *widget) const override;

    // dumb thick margins
    int pixelMetric(QStyle::PixelMetric metric, const QStyleOption *option = nullptr, const QWidget *widget = nullptr) const override;
};

